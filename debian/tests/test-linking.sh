#!/bin/sh

set -e

cd debian/tests
g++ $(pkg-config --cflags --libs systemc) test.cpp
./a.out
rm -f a.out

